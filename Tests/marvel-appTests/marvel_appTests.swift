    import XCTest
    @testable import marvel_app

    final class marvel_appTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(marvel_app().text, "Hello, World!")
        }
    }
