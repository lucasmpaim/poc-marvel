[![Tuist badge](https://img.shields.io/badge/Powered%20by-Tuist-blue)](https://tuist.io)


## Modularização
[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVEQ7XG5cbkFbW0FwcF1dO1xuQltBcHBDb3JlXTtcbkNbQXBwQ29yZVVJXTtcbkRbRmVhdHVyZV07XG5IW1tVSURlbW9dXTtcbkpbW0ZlYXR1cmVEZW1vXV07XG5LW05ldHdvcmtdO1xuTFtSZXBvc2l0b3J5XTtcblxuQi0tPkM7XG5DLS0-RDtcbkQtLT5BO1xuQy0tPkg7XG5ELS0-SjtcbkwtLT5EO1xuSy0tPkw7XG5CLS0-SztcbiIsIm1lcm1haWQiOnt9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVEQ7XG5cbkFbW0FwcF1dO1xuQltBcHBDb3JlXTtcbkNbQXBwQ29yZVVJXTtcbkRbRmVhdHVyZV07XG5IW1tVSURlbW9dXTtcbkpbW0ZlYXR1cmVEZW1vXV07XG5LW05ldHdvcmtdO1xuTFtSZXBvc2l0b3J5XTtcblxuQi0tPkM7XG5DLS0-RDtcbkQtLT5BO1xuQy0tPkg7XG5ELS0-SjtcbkwtLT5EO1xuSy0tPkw7XG5CLS0-SztcbiIsIm1lcm1haWQiOnt9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)


A ideia por trás desta modularização é ter um nível maior de separação de responsabilidades e otimização do build da aplicação, uma vez que é possível rodar features da aplicação de forma isolada.

Obs: Este é um diagrama geral da arquitetura, a implementação segue estas premissas, mas tem módulos extras. Os módulos foram criados utilizando SPM (Swift Package Manager).

Também fiquei sem tempo hábil para criar os app's demos para mostrar apenas uma funcionalidade, mas a ideia seria criar um "component book" para demonstrar todo o design system do app. Também criar app's para rodar funcionalidades de forma isolada do restante do aplicativo.


--------------


## Execução

Para executar o aplicativo é preciso criar um arquivo `Configurations/iOS/MarvelConfig.xcconfig` o conteúdo deste arquivo deve ser:

```
MARVEL_API_PUBLIC_KEY =
MARVEL_API_PRIVATE_KEY =
```

Além disso é necessário ter instalado a ferramenta [tuist](https://tuist.io/), para abrir o projeto basta executar:

```bash
tuist focus App
```
