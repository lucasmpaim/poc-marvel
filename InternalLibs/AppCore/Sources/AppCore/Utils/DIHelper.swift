//
//  File.swift
//  
//
//  Created by Lucas Paim on 23/05/21.
//

import Foundation
import Swinject

public protocol DIHelper {
    static func registerServicesInContainer(container: Container)
}
