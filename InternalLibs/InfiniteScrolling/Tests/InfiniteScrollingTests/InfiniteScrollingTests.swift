    import XCTest
    @testable import InfiniteScrolling

    final class InfiniteScrollingTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(InfiniteScrolling().text, "Hello, World!")
        }
    }
