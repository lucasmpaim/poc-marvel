    import XCTest
    @testable import AppCoreUI

    final class AppCoreUITests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(AppCoreUI().text, "Hello, World!")
        }
    }
