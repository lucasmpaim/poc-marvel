//
//  File.swift
//  
//
//  Created by Lucas Paim on 23/05/21.
//

import Foundation

public enum RepositoryErrors: Error {
    case invalidService
}
