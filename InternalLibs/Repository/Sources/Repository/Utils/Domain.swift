//
//  File.swift
//  
//
//  Created by Lucas Paim on 22/05/21.
//

import Foundation


/**
 Protocol to define that object it's from domain
 */
public protocol Domain { }
