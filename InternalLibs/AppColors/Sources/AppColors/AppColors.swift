

import UIKit


public extension UIColor {
    
    static var appBackgroundColor = UIColor(named: "appBackground")
    static var secondaryBackgroundColor = UIColor(named: "secondaryBackground")
    static var textColor = UIColor(named: "textColor")
    static var contrastColor = UIColor(named: "contrastColor")
    static var textColorInContrast = UIColor(named: "textColorInContrast")

}
