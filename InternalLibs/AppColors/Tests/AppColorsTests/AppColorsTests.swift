    import XCTest
    @testable import AppColors

    final class AppColorsTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(AppColors().text, "Hello, World!")
        }
    }
