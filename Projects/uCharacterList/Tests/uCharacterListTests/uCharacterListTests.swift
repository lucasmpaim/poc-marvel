    import XCTest
    @testable import uCharacterList

    final class uCharacterListTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(uCharacterList().text, "Hello, World!")
        }
    }
