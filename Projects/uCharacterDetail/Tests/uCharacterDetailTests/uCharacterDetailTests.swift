    import XCTest
    @testable import uCharacterDetail

    final class uCharacterDetailTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(uCharacterDetail().text, "Hello, World!")
        }
    }
